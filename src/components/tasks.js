import React from "react";
import { List, Datagrid, TextField, EmailField } from "react-admin";
import MyUrlField from "./myUrlField";

export const TaskList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="task_id" />
      <TextField source="task_name" />
      <TextField source="task_description" />
    </Datagrid>
  </List>
);
