import React from "react";
import {
  List,
  Datagrid,
  RichTextField,
  TextField,
  ReferenceField,
  NumberField,
  Edit,
  SimpleForm,
  EditButton,
  SelectInput,
  Create,
  TextInput,
  NumberInput,
  BooleanInput,
  ReferenceInput,
  DateInput,
  AutocompleteInput
} from "react-admin";

import RichTextInput from "ra-input-rich-text";

const DealTitle = ({ record }) => {
  return <span>Deal: {record ? `${record.deal_name}` : ""} </span>;
};

export const DealList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <ReferenceField source="id" reference="deals" link={false}>
        <TextField source="id" />
      </ReferenceField>
      <TextField source="deal_name" />
      <ReferenceField
        label="Sector"
        source="sector"
        reference="sectors"
        link="show"
      >
        <TextField source="sector_name" />
      </ReferenceField>
      <TextField source="type" />
      <NumberField source="amount" />
      <ReferenceField
        label="Currency"
        source="currency"
        reference="currencies"
        link={false}
      >
        <TextField source="currency_name" />
      </ReferenceField>
      <ReferenceField
        label="Assigned To"
        source="assigned"
        reference="users"
        link={false}
      >
        <TextField source="first_name" />
      </ReferenceField>
    </Datagrid>
  </List>
);

export const DealEdit = props => (
  <Edit title={<DealTitle />} {...props}>
    <SimpleForm>
      <TextInput source="deal_name" />
      <RichTextInput multiline source="deal_description" />
      <ReferenceInput perPage={-1} label="Country" source="country" reference="countries">
        <SelectInput optionText="country_name" />
      </ReferenceInput>
      <TextInput source="type" />
      <TextInput source="investor_type" />
      <BooleanInput source="board_seat" />
      <NumberInput source="impact_category" />
      <BooleanInput source="impact" />
      <NumberInput source="assigned" />
      <NumberInput source="impact_theme" />
      <TextInput source="status" />
      <NumberInput source="impact_tolerance" />
      <TextInput source="notes" />
      <TextInput source="syndication" />
    </SimpleForm>
  </Edit>
);
