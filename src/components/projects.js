import React from "react";
import {
  List,
  Datagrid,
  RichTextField,
  TextField,
  ReferenceField,
  DateField,
  NumberField,
  Edit,
  SimpleForm,
  EditButton,
  SelectInput,
  Create,
  TextInput,
  NumberInput,
  BooleanInput,
  ReferenceInput,
  DateInput,
  AutocompleteInput
} from "react-admin";

export const ProjectList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="project_name" />
      <TextField source="status" />
      <NumberField source="assigned" />
      <TextField source="details" />
      <NumberField source="created_by" />
      <DateField source="due" />
      <DateField source="created_date" />
      <TextField source="id" />
    </Datagrid>
  </List>
);

export const ProjectEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DateInput source="project_name" />
      <TextInput source="status" />
      <NumberInput source="assigned" />
      <TextInput source="details" />
      <TextInput source="created_by" />
      <DateInput source="due" />
      <TextInput source="created_date" />
      <TextInput source="id" />
    </SimpleForm>
  </Edit>
);
