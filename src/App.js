import React from "react";
import {
  fetchUtils,
  Admin,
  Resource,
  ListGuesser,
  EditGuesser
} from "react-admin";
import authProvider from "./helpers/authProvider";
import { TaskList } from "./components/tasks";
import Cookies from "./helpers/Cookies";

import restDataProvider from "./helpers/ra-directus-rest";
import { DealList, DealEdit } from "./components/deals";
import { ProjectList, ProjectEdit } from "./components/projects";

const apiUrl = "http://platform.andsimple.co/demo";

const httpClient = (url, options = {}) => {
  if (!options.headers) {
    options.headers = new Headers({ Accept: "application/json" });
  }
  const token = Cookies.getCookie("token");
  options.headers.set("Authorization", `Bearer ${token}`);
  return fetchUtils.fetchJson(url, options);
};

const dataProvider = restDataProvider(apiUrl, httpClient);

const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider}>
    {/* Invisible resources for loading tables */}
    <Resource name="sectors" />
    <Resource name="countries" />
    <Resource name="currencies" />
    <Resource name="users" />

    <Resource name="tasks" list={TaskList}></Resource>
    <Resource name="deals" list={DealList} edit={DealEdit}></Resource>
    <Resource
      name="investments"
      list={ListGuesser}
      edit={EditGuesser}
    ></Resource>
    <Resource name="projects" list={ProjectList} edit={ProjectEdit}></Resource>
  </Admin>
);

export default App;
