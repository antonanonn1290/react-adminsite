import { fetchUtils } from "react-admin";
import { stringify } from "query-string";

const updateUrlForDirectus = (apiUrl, resource) => {
  const supportedResourceEndpoints = ["users", "roles", "files"];
  const unsupportedResourceEndpoints = [
    "revisions",
    "relations",
    "settings",
    "folders"
  ];

  if (supportedResourceEndpoints.includes(resource)) {
    return apiUrl;
  } else if (unsupportedResourceEndpoints.includes(resource)) {
    throw Error(`Unsupported API endpoint: ${apiUrl}/${resource}`);
  }
  return apiUrl + "/items";
};

const adjustQueryForDirectus = (resource, params) => {
  /*
        params = {
            pagination: { page: {int} , perPage: {int} },
            sort: { field: {string}, order: {string} },
            filter: {Object},
            target: {string}, (REFERENCE ONLY)
            id: {mixed} (REFERENCE ONLY)
        }
        */

  /*

    Handle SORTING

    TODO: Support multiple sort fields

    From Directus docs:
    sort is a CSV of fields used to sort the fetched items.
    Sorting defaults to ascending (ASC) order but a minus sign (-) can be used to reverse this to descending (DESC) order.
    Fields are prioritized by their order in the CSV. You can also use a ? to sort randomly

    Examples:
    # Sorts randomly
    ?sort=?

    # Sorts by name ASC
    ?sort=name

    # Sorts by name ASC, followed by age DESC
    ?&sort=name,-age

    # Sorts by name ASC, followed by age DESC, followed by random sorting
    ?sort=name,-age,?
    */
  const s = params.sort;
  const sort =
    "sort=" + (s.order === "ASC" ? "-" : "") + (s.field === "" ? "?" : s.field);

  /*

    HANDLE FILTER

    Used to search items in a collection that matches the filter's conditions.
    Filters follow the syntax filter[<field-name>][<operator>]=<value>.

    The field-name supports dot-notation to filter on nested relational fields

    */

  const filter = "";

  // DIRECTUS IMPLEMENTATION
  // const f = params.filter;
  // const filter_keys = Object.keys(f);
  // let filter = ''
  // for (let i =0; i < keys.length; i++) {
  //     // react-admin uses the 'q' string to indicate filter
  //     if (keys[i] == 'q' && f[keys[i] !== ""]) {
  //         filter += `filter[${keys[i]}][]`
  //     }
  // }

  // ======

  // STRAPI IMPLEMENATION
  // const f = params.filter;
  // let filter = "";
  // const keys = Object.keys(f);
  // for (let i = 0; i < keys.length; i++) {
  //   //react-admin uses q filter in several components and strapi use _q
  //   if (keys[i] === "q" && f[keys[i]] !== "") {
  //     filter += "_q=" + f[keys[i]] + (keys[i + 1] ? "&" : "");
  //   } else {
  //     filter += keys[i] + "=" + f[keys[i]] + (keys[i + 1] ? "&" : "");
  //   }
  // }
  // if (params.id && params.target && params.target.indexOf("_id") !== -1) {
  //   const target = params.target.substring(0, params.target.length - 3);
  //   filter += "&" + target + "=" + params.id;
  // }

  // Handle PAGINATION
  const { page, perPage } = params.pagination;
  const start = (page - 1) * perPage;
  const limit = perPage; // for directus the limit param indicate the amount of elements to return in the response
  const range = "page=" + start + "&limit=" + limit;

  return sort + "&" + range + "&" + filter;
};

export default (apiUrl, httpClient) => ({
  getList: (resource, params) => {
    const query = adjustQueryForDirectus(resource, params);
    const url = `${updateUrlForDirectus(
      apiUrl,
      resource
    )}/${resource}?${query}`;

    return httpClient(url).then(({ json }) => ({
      data: json.data.map(obj => ({ ...obj, id: obj["id"] })),
      total: json.data.length
    }));
  },

  getOne: (resource, params) => {
    return httpClient(
      `${updateUrlForDirectus(apiUrl, resource)}/${resource}/${params.id}`
    ).then(({ json }) => {
      return {
        data: json.data
      };
    });
  },

  getMany: (resource, params) => {
    const url = `${updateUrlForDirectus(
      apiUrl,
      resource
    )}/${resource}?${params.ids.join(",")}`;

    console.log("getMany: ", url);
    console.log("params", params);
    return httpClient(url).then(({ json }) => {
      console.log("returned data:", json);
      return { data: json.data };
    });
  },

  getManyReference: (resource, params) => {
    // TODO: Implement relational filtering
    const query = adjustQueryForDirectus(resource, params);
    console.log("getManyReference", query);
    console.log("params: ", params);

    const url = `${updateUrlForDirectus(
      apiUrl,
      resource
    )}/${resource}?${stringify(query)}`;

    return httpClient(url).then(({ json }) => ({
      data: json.data.map(obj => ({ ...obj, id: obj["id"] })),
      total: json.data.length
    }));
  },

  update: (resource, params) =>
    httpClient(
      `${updateUrlForDirectus(apiUrl, resource)}/${resource}/${params.id}`,
      {
        method: "PATCH",
        body: JSON.stringify(params.data)
      }
    ).then(({ json }) => ({ data: json.data })),

  updateMany: (resource, params) => {
    const query = adjustQueryForDirectus(resource, params);

    const {
      created_at,
      updated_at,
      createdAt,
      updatedAt,
      ...data
    } = params.data;

    return httpClient(
      `${updateUrlForDirectus(apiUrl, resource)}/${resource}?${query}`,
      {
        method: "PATCH",
        body: JSON.stringify(data)
      }
    ).then(({ json }) => ({ data: json }));
  },

  create: (resource, params) =>
    httpClient(`${apiUrl}/${resource}`, {
      method: "POST",
      body: JSON.stringify(params.data)
    }).then(({ json }) => ({
      data: { ...params.data, id: json.id }
    })),

  delete: (resource, params) =>
    httpClient(`${apiUrl}/${resource}/${params.id}`, {
      method: "DELETE"
    }).then(({ json }) => ({ data: json })),

  deleteMany: (resource, params) => {
    const query = {
      filter: JSON.stringify({ id: params.ids })
    };
    return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
      method: "DELETE",
      body: JSON.stringify(params.data)
    }).then(({ json }) => ({ data: json }));
  }
});
