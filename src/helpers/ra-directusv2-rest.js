import {
  fetchUtils,
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  UPDATE_MANY,
  DELETE,
  DELETE_MANY
} from "react-admin";

/**
 * Maps react-admin queries to a simple REST API
 *
 * The REST dialect is similar to the one of FakeRest
 * TODO
 * GET_LIST     => GET
 * GET_ONE      => GET
 * GET_MANY     => GET
 * UPDATE       => PUT
 * CREATE       => POST
 * DELETE       => DELETE
 */
export default (apiUrl, httpClient = fetchUtils.fetchJson) => {
  const updateUrlForDirectus = (apiUrl, resource) => {
    const supportedResourceEndpoints = ["users", "roles", "files"];
    const unsupportedResourceEndpoints = [
      "revisions",
      "relations",
      "settings",
      "folders"
    ];

    if (supportedResourceEndpoints.includes(resource)) {
      return apiUrl;
    } else if (unsupportedResourceEndpoints.includes(resource)) {
      throw Error(`Unsupported API endpoint: ${apiUrl}/${resource}`);
    }
    return apiUrl + "/items";
  };

  /**
   * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
   * @param {String} resource Name of the resource to fetch, e.g. 'posts'
   * @param {Object} params The data request params, depending on the type
   * @returns {Object} { url, options } The HTTP request parameters
   */
  const convertDataRequestToHTTP = (type, resource, params) => {
    let url = "";
    const options = {};
    switch (type) {
      case GET_LIST:
        url = `${updateUrlForDirectus(
          apiUrl,
          resource
        )}/${resource}?${adjustQueryForDirectus(resource, params)}`;
        break;
      case GET_MANY_REFERENCE:
        url = `${updateUrlForDirectus(
          apiUrl,
          resource
        )}/${resource}?${adjustQueryForDirectus(resource, params)}`;
        break;
      case GET_ONE:
        url = `${updateUrlForDirectus(apiUrl, resource)}/${resource}/${
          params.id
        }`;
        break;
      case GET_MANY:
        url = `${updateUrlForDirectus(
          apiUrl,
          resource
        )}/${resource}/${params.ids.join(",")}`;
        break;
      case UPDATE:
        url = `${updateUrlForDirectus(apiUrl, resource)}/${resource}/${
          params.id
        }`;
        options.method = "PATCH";
        // Omit created_at/updated_at(RDS) and createdAt/updatedAt(Mongo) in request body
        const {
          created_at,
          updated_at,
          createdAt,
          updatedAt,
          ...data
        } = params.data;
        options.body = JSON.stringify(data);
        break;
      case CREATE:
        url = `${apiUrl}/${resource}`;
        options.method = "POST";
        options.body = JSON.stringify(params.data);
        break;
      case DELETE:
        url = `${apiUrl}/${resource}/${params.id}`;
        options.method = "DELETE";
        break;
      default:
        throw new Error(`Unsupported fetch action type ${type}`);
    }
    return { url, options };
  };

  const adjustQueryForDirectus = (resource, params) => {
    /*
        params = { 
            pagination: { page: {int} , perPage: {int} }, 
            sort: { field: {string}, order: {string} }, 
            filter: {Object}, 
            target: {string}, (REFERENCE ONLY)
            id: {mixed} (REFERENCE ONLY)
        }
        */

    /*

    Handle SORTING

    TODO: Support multiple sort fields

    From Directus docs:
    sort is a CSV of fields used to sort the fetched items. 
    Sorting defaults to ascending (ASC) order but a minus sign (-) can be used to reverse this to descending (DESC) order. 
    Fields are prioritized by their order in the CSV. You can also use a ? to sort randomly

    Examples:
    # Sorts randomly
    ?sort=?

    # Sorts by name ASC
    ?sort=name

    # Sorts by name ASC, followed by age DESC
    ?&sort=name,-age

    # Sorts by name ASC, followed by age DESC, followed by random sorting
    ?sort=name,-age,?
    */
    const s = params.sort;
    const sort = "sort=" +
      (s.order === "ASC" ? "-" : "") +
      (s.field === "" ? "?" : s.field);

    /*

    HANDLE FILTER

    Used to search items in a collection that matches the filter's conditions. 
    Filters follow the syntax filter[<field-name>][<operator>]=<value>. 
    
    The field-name supports dot-notation to filter on nested relational fields

    */

    const filter = "";

    // DIRECTUS IMPLEMENTATION
    // const f = params.filter;
    // const filter_keys = Object.keys(f);
    // let filter = ''
    // for (let i =0; i < keys.length; i++) {
    //     // react-admin uses the 'q' string to indicate filter
    //     if (keys[i] == 'q' && f[keys[i] !== ""]) {
    //         filter += `filter[${keys[i]}][]`
    //     }
    // }

    // ======

    // STRAPI IMPLEMENATION
    // const f = params.filter;
    // let filter = "";
    // const keys = Object.keys(f);
    // for (let i = 0; i < keys.length; i++) {
    //   //react-admin uses q filter in several components and strapi use _q
    //   if (keys[i] === "q" && f[keys[i]] !== "") {
    //     filter += "_q=" + f[keys[i]] + (keys[i + 1] ? "&" : "");
    //   } else {
    //     filter += keys[i] + "=" + f[keys[i]] + (keys[i + 1] ? "&" : "");
    //   }
    // }
    // if (params.id && params.target && params.target.indexOf("_id") !== -1) {
    //   const target = params.target.substring(0, params.target.length - 3);
    //   filter += "&" + target + "=" + params.id;
    // }

    // Handle PAGINATION
    const { page, perPage } = params.pagination;
    const start = (page - 1) * perPage;
    const limit = perPage; // for directus the limit param indicate the amount of elements to return in the response
    const range = "page=" + start + "&limit=" + limit;

    return sort + "&" + range + "&" + filter;
  };

  /**
   * @param {Object} response HTTP response from fetch()
   * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
   * @param {String} resource Name of the resource to fetch, e.g. 'posts'
   * @param {Object} params The data request params, depending on the type
   * @returns {Object} Data response
   */
  const convertHTTPResponse = (response, type, resource, params) => {
    const { json } = response;

    const id_field = "id";
    switch (type) {
      case GET_LIST:
        return {
          data: json.data.map(obj => ({ ...obj, id: obj[id_field] })),
          total: json.data.length
        };
      case GET_MANY_REFERENCE:
        return {
          data: json.data.map(obj => ({ ...obj, id: obj[id_field] })),
          total: json.data.length
        };
      case CREATE:
        return { data: { ...params.data, id: json.data[id_field] } };
      case DELETE:
        return { data: { id: json.data[id_field] } };
      default:
        return { data: { ...json.data, id: json.data[id_field] } };
    }
  };

  /**
   * @param {string} type Request type, e.g GET_LIST
   * @param {string} resource Resource name, e.g. "posts"
   * @param {Object} payload Request parameters. Depends on the request type
   * @returns {Promise} the Promise for a data response
   */
  return (type, resource, params) => {
    /*
    TODO: Refactor this to be better compatible with directus. 
    No need to do n calls.

    See https://docs.directus.io/api/reference.html#get-items
    */

    if (type === UPDATE_MANY) {
      return Promise.all(
        params.ids.map(id => {
          // This ommits the date information from the returned data
          const { created_at, createdAt, ...data } = params.data;
          return httpClient(`${apiUrl}/${resource}/${id}`, {
            method: "PATCH",
            body: JSON.stringify(data)
          });
        })
      ).then(responses => ({
        data: responses.map(response => response.json)
      }));
    }
    if (type === DELETE_MANY) {
      return Promise.all(
        params.ids.map(id =>
          httpClient(`${apiUrl}/${resource}/${id}`, {
            method: "DELETE"
          })
        )
      ).then(responses => ({
        data: responses.map(response => response.json)
      }));
    }
    // if (type === GET_MANY) {
    //   return Promise.all(
    //     params.ids.map(id =>
    //       httpClient(
    //         `${updateUrlForDirectus(apiUrl, resource)}/${resource}/${id}`,
    //         {
    //           method: "GET"
    //         }
    //       )
    //     )
    //   ).then(responses => ({
    //     data: responses.map(response => response.json)
    //   }));
    // }

    const { url, options } = convertDataRequestToHTTP(type, resource, params);

    return httpClient(url, options).then(response =>
      convertHTTPResponse(response, type, resource, params)
    );
  };
};
